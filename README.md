## springboot + mybatisPlus 多数据源配置实例

### 1、思路

- yml中配置多个数据源
- 通过AOP切面控制，动态切换不同数据源
- 配合Druid和mybatisPlus使用

### 2、关键代码

##### yml文件

```yaml
server:
  port: 8080
  #address: 127.0.0.1
  #sessionTimeout: 30
  servlet:
    context-path: /
  tomcat:
    uri-encoding: UTF-8

spring:
  aop:
    proxy-target-class: true
    auto: true
  datasource:
    druid:
      # 配置多个数据源
      # db1 使用mysql8.0
      db1:
        url: jdbc:mysql://localhost:3306/test?zeroDateTimeBehavior=convertToNull&characterEncoding=utf8&useSSL=false&serverTimezone=UTC
        username: root
        password: 123456
        driver-class-name: com.mysql.cj.jdbc.Driver
        initialSize: 5
        minIdle: 5
        maxActive: 20
      # db2 使用mysql5.7
      db2:
        url: jdbc:mysql://192.168.1.126:3306/test?characterEncoding=utf8&useSSL=false
        username: root
        password: 123456
        driver-class-name: com.mysql.cj.jdbc.Driver
        initialSize: 5
        minIdle: 5
        maxActive: 20
```

##### MybatisPlusConfig

```java
package com.example.demo.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.example.demo.common.DBTypeEnum;
import com.example.demo.common.DynamicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * mybatisPlus配置类
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.example.demo.mapper.db*")
public class MybatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        return paginationInterceptor;
    }

    @Bean(name = "db1")
    @ConfigurationProperties(prefix = "spring.datasource.druid.db1")
    public DataSource db1() {
        return DruidDataSourceBuilder.create().build();
    }

    @Bean(name = "db2")
    @ConfigurationProperties(prefix = "spring.datasource.druid.db2")
    public DataSource db2() {
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * 动态数据源配置
     * @return
     */
    @Bean
    @Primary
    public DataSource multipleDataSource(@Qualifier("db1") DataSource db1, @Qualifier("db2") DataSource db2) {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DBTypeEnum.db1.getValue(), db1);
        targetDataSources.put(DBTypeEnum.db2.getValue(), db2);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        dynamicDataSource.setDefaultTargetDataSource(db1);
        return dynamicDataSource;
    }

    @Bean("sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean sqlSessionFactory = new MybatisSqlSessionFactoryBean();
        sqlSessionFactory.setDataSource(multipleDataSource(db1(), db2()));
        /**
         * 下面这一句setMapperLocations必须加，不然会报错：invalid bound statement (not found)问题
         */
        sqlSessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/mappers/*.xml"));
        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setCacheEnabled(false);
        sqlSessionFactory.setConfiguration(configuration);
        //添加分页功能
        sqlSessionFactory.setPlugins(paginationInterceptor());
        return sqlSessionFactory.getObject();
    }

}
```

##### DataSourceSwitchAspect切面类

```java
package com.example.demo.common;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 动态数据源AOP切面类
 */
@Component
@Order(value = -100)
@Slf4j
@Aspect
public class DataSourceSwitchAspect {

    @Pointcut("execution(* com.example.demo.mapper.db1..*.*(..))")
    private void db1Aspect() {
    }

    @Pointcut("execution(* com.example.demo.mapper.db2..*.*(..))")
    private void db2Aspect() {
    }

    @Before("db1Aspect()")
    public void db1() {
        log.info("切换到db1 数据源...");
        DbContextHolder.setDbType(DBTypeEnum.db1);
    }

    @Before("db2Aspect()")
    public void db2() {
        log.info("切换到db2 数据源...");
        DbContextHolder.setDbType(DBTypeEnum.db2);
    }

}
```

##### 测试类

```java
package com.example.demo.controller;

import com.example.demo.mapper.db1.UserMapper;
import com.example.demo.mapper.db2.DictMapper;
import com.example.demo.model.Dict;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DictMapper dictMapper;

    /**
     * 同时查询2个数据库
     * @param id
     * @return
     */
    @GetMapping("/get")
    public String getUserById(){
        User user = userMapper.findUserByName("aaa");
        Dict dict = dictMapper.findNameByCode("001");
        return "hello " + user.getName() + ": " + dict.getName();
    }

}
```

### 3、运行结果

![输入图片说明](https://images.gitee.com/uploads/images/2020/0901/141232_4604354c_367913.png "Y91K})(1Z0[0F{029RT{73W.png")

### 4.  写在最后

本实例源代码：https://gitee.com/jelly_oy/springboot-mybatisPlus-multiDatasource

本实例采用springboot2.3.3 + mybatisPlus3.3.2 + 多数据源配置，相关数据库脚本也在项目路径下，可以直接运行。

如果本项目对你有帮助，欢迎留言评论，欢迎git clone源代码，  

也欢迎对本项目进行捐助，你的鼓励将是我开源创造的动力！  


![输入图片说明](https://images.gitee.com/uploads/images/2020/0817/120247_e39e4fe7_367913.png "a6x0079437ysmajwki57y16.png")
