package com.example.demo.controller;

import com.example.demo.mapper.db1.UserMapper;
import com.example.demo.mapper.db2.DictMapper;
import com.example.demo.model.Dict;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DictMapper dictMapper;

    /**
     * 同时查询2个数据库
     * @param
     * @return
     */
    @GetMapping("/get")
    public String getUserById(){
        User user = userMapper.findUserByName("aaa");
        Dict dict = dictMapper.findNameByCode("001");
        return "hello " + user.getName() + ": " + dict.getName();
    }

}
