package com.example.demo.mapper.db2;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.model.Dict;

public interface DictMapper extends BaseMapper<Dict> {
    public Dict findNameByCode(String code);

}
