/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.1.126
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 192.168.1.126:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 01/09/2020 14:04:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for db_dict
-- ----------------------------
DROP TABLE IF EXISTS `db_dict`;
CREATE TABLE `db_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of db_dict
-- ----------------------------
INSERT INTO `db_dict` VALUES (1, '张三', '001');
INSERT INTO `db_dict` VALUES (2, '李四', '002');
INSERT INTO `db_dict` VALUES (3, '王五', '003');

SET FOREIGN_KEY_CHECKS = 1;
